package cn.le.conditions;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.le.page.DataRequest;
import cn.le.util.StaticField;


/**
 *<P>操作语句处理基类<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
@Component("org_handle")
@Scope("prototype")
public class Handle {

	/** 开始条数*/
	protected Integer startCount;
	/** 结束条数*/
	protected Integer endCount;
	/** 分页查询语句*/
	protected StringBuffer pagingSentence;
	/** 总记录数查询语句*/
	protected StringBuffer countSentence;
	/** 合计查询语句*/
	protected StringBuffer sumSentence;
	/** 操作语句 */
	protected StringBuffer executeSentence;
	/** 操作语句对应参数 */
	protected List<Object> paramsArray;
	/**
	 *传入参数
	 * @param dataRequest 分页参数集
	 */
	public void  setPageRequest(DataRequest dataRequest)
	{
		paramHandle(dataRequest);
		//整理查询语句
		pageHandle(dataRequest);
	}
	/**
	 * 计算分页数据
	 * @param dataRequest
	 */
	protected void paramHandle(DataRequest dataRequest)
	{
		//计算开始条数
		 startCount = (dataRequest.getPageNumber() - 1) * dataRequest.getPageSize();
		//计算结束条数
		 endCount = startCount + dataRequest.getPageSize();
	}
	/**
	 * page 合计 处理
	 */
	protected void pageHandle(DataRequest dataRequest)
	{
		//分页操作 
		pagingSentence = new StringBuffer("SELECT * FROM ( SELECT A.*, ROWNUM RN  FROM (  ")
			.append(dataRequest.getFieldsColumns()).append(dataRequest.getSortColumns()).append(dataRequest.getGroupColumns())
			.append(" ) A WHERE ROWNUM <= ").append(endCount)
			.append(" ) WHERE RN > ").append(startCount);
		
		//处理查询总行数sql
		countSentence = new StringBuffer();
		countSentence.append("select count(").append(dataRequest.getParamCount()).append(") count FROM (").append(dataRequest.getFieldsColumns())
		.append(")");		
		
		//处理合计sql
		if(StringUtils.isNotEmpty(dataRequest.getSumColumn()))
		{
			sumSentence = new StringBuffer("SELECT");
			String[] columns = dataRequest.getSumColumn().split(",");
			for (int i = 0,size = columns.length; i < size; i++) {
				sumSentence.append(StaticField.SPACE).append("sum(").append(columns[i]).append(")").append(StaticField.SPACE).append(columns[i]);
				if( i < size -1)
				{
					sumSentence.append(",");
				}
			}
			sumSentence.append(StaticField.SPACE).append("FROM (").append(dataRequest.getFieldsColumns()).append(")");
		}
	}
	
	/**
	 * 处理新增
	 * @param params参数集
	 * @param tableName 表名
	 */
	public void insertHand(Map<String,Object> params,String tableName)
	{
		paramsArray = new ArrayList<Object>();
		executeSentence = new StringBuffer("insert into ").append(tableName);
		//需要新增的列
		StringBuffer left = new StringBuffer(" ( ");
		//新增列对象的值
		StringBuffer right = new StringBuffer(" ( ");
		//处理参数集
		int index = 1;
		//参数集长度
		int size =  params.size();
		for (Map.Entry<String, Object>  param: params.entrySet()) {
			left.append(param.getKey());
			right.append("?");
			if(index < size)
			{
				left.append(",");
				right.append(",");
				index ++;
			}
			paramsArray.add(param.getValue());
		}
		left.append(")");
		right.append(")");
		executeSentence.append(left).append(" values ").append(right);
	}
	/**
	 * 处理修改
	 * @param params参数集
	 * @param tableName 表名
	 */
	public void updateHand(Map<String, Object> columnParameter,String tableName, Map<String, Object> whereMap) {
			
		paramsArray = new ArrayList<Object>();
		executeSentence = new StringBuffer("update ").append(tableName).append(" set ");
		//处理参数集
		int index = 1;
		 for(Map.Entry<String, Object> param : columnParameter.entrySet())
		 {
			 executeSentence.append(param.getKey()).append(" = ? ");
			 if(index < columnParameter.size())
			 {
				 executeSentence.append(",");
				 index ++;
			 }
			 paramsArray.add(param.getValue());
		 }
		//处理条件集
		 index = 1;
		 executeSentence.append(" where ");
		 for(Map.Entry<String, Object> param : whereMap.entrySet())
		 {
			 executeSentence.append(param.getKey()).append(" = ? ");
			 if(index < whereMap.size())
			 {
				 executeSentence.append(" and ");
				 index ++;
			 }
			 paramsArray.add(param.getValue());
		 }
	}
   /**
	* 处理删除
	* @param  columnParameter
	* @param  tableName
	* @param  whereMap    
	* @return void    
	 */
	public void deleteHand(String tableName, Map<String, Object> whereMap)
	{
		paramsArray = new ArrayList<Object>();
		executeSentence = new StringBuffer("delete ").append(tableName);
		
		//处理条件集
		int index = 1;
		 executeSentence.append(" where ");
		 for(Map.Entry<String, Object> param : whereMap.entrySet())
		 {
			 executeSentence.append(param.getKey()).append(" = ? ");
			 if(index < whereMap.size())
			 {
				 executeSentence.append(" and ");
				 index ++;
			 }
			 paramsArray.add(param.getValue());
		 }
	}
	/**
	 * 获取分页查询语句
	 * @return
	 */
	public String getPagingSentence() {
		return pagingSentence.toString();
	}
	/**
	 * 获取总记录数查询语句
	 * @return
	 */
	public String getCountSentence() {
		return countSentence.toString();
	}
	/**
	 * 获取合计查询语句
	 * @return
	 */
	public String getSumSentence() {
		return sumSentence.toString();
	}
	/**
	 * 获取操作语句
	 * @return
	 */
	public String getExecuteSentence() {
		return executeSentence.toString();
	}
	/**
	 * 获取操作语句对应的值
	 * @return
	 */
	public Object[] getParamsArray() {
		return paramsArray.toArray();
	}
}
