package cn.le.conditions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import cn.le.util.StaticField;
import cn.le.util.Tools;


/**
 *<P>处理request中的参数类<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
public class QueryConditions {

	/**
	 * 页面查询列
	 */
	private StringBuffer queryCriteria;
	/**
	 * 页面查询参数
	 */
	private List<Object> params;
	/**
	 * 修改添加map
	 */
	private Map<String,Object> whereMap;
	
	/**
	 * 处理页面非分页参数
	 * @param requestParam input标签中name="org_name[type]" and name="org_name[type][where]"参数封装的map集合
	 * @return column-value
	 */
	public Map<String,Object> getColumnParameter(Map<String,Object> requestParam)
	{
		
		Map<String,Object> result = new HashMap<String,Object>();		
		whereMap = new HashMap<String,Object>();
		
		for (Map.Entry<String, Object> param : requestParam.entrySet()) {
			
			String fieldName = param.getKey();
			Object fieldValue = param.getValue();
			if(StringUtils.isEmpty(fieldValue.toString())){ continue; }
			//columns
			String key = "";
			//匹配页面参数
			// 具有类型参数条件 Columns[type][where]
			Pattern patterTwo = Pattern.compile("(.+?)\\[(.+)]\\[(.+)]");
			Matcher maecherTwo = patterTwo.matcher(fieldName);
			if(maecherTwo.matches())
			{
				//不是where条件参数直接返回不处理
				if(!fieldName.contains("where"))
				{
					continue;
				}
				key = maecherTwo.group(1);
				String type = maecherTwo.group(2);
				fieldValue = Tools.convert(fieldValue, type);
				whereMap.put(key, fieldValue);
			}
			else
			{
				// 具有类型参数条件 Columns[type]
				Pattern pattern = Pattern.compile("(.+?)\\[(.+)]");
				//配页面参数和类型
				Matcher maecher = pattern.matcher(fieldName);
				if(maecher.matches())
				{
					key = maecher.group(1);
					String type = maecher.group(2);
					if("no".equalsIgnoreCase(type))
					{
						continue;
					}
					fieldValue = Tools.convert(fieldValue, type);
					result.put(key.trim(), fieldValue);
				}
			}
		}
		return result;
	}
	
	/**
	 * 处理页面分页参数
	 * @param requestParam requestParam input标签中name="org_name[=][int]"参数封装的map集合
	 * @return
	 */
	public void requestColumns(Map<String,Object> requestParam)
	{
		
		//初始化查询条件
		queryCriteria = new StringBuffer();
		params = new ArrayList<Object>();
		/*
		 * 解析参数集,装载分页, 查询参数
		 */
		//循环下标
		int index = 0;
		//参数集最大下标
		int size = requestParam.size();
		for (Map.Entry<String, Object> parameter : requestParam.entrySet()) {
			index ++;
			String fieldName = parameter.getKey();
			Object fieldValue = parameter.getValue();
			//不处理没有值的参数
			if(StringUtils.isEmpty(fieldValue.toString())){ continue; }
			//columns
			String key = "";
			//数据类型
			String type = "";
			//关系符
			String operator = "";
			// 具有类型参数条件 Columns[=][int]
			Pattern patterTwo = Pattern.compile("(.+?)\\[(.+)]\\[(.+)]");
			Matcher maecherTwo = patterTwo.matcher(fieldName);
			if(maecherTwo.matches())
			{
				key = maecherTwo.group(1);
				operator = maecherTwo.group(2);
				type = maecherTwo.group(3);
				//处理in , not in 关系符  begin
				if(StaticField.IN.equalsIgnoreCase(operator) || StaticField.NOTIN.equalsIgnoreCase(operator))
				{
					handleContain(fieldValue.toString(), key, operator, type);
					continue;
				}
				//处理in , not in 关系符  end
				//处理like 关系符 begin
				if(StaticField.LIKE.equalsIgnoreCase(operator))
				{
					handleVague(fieldValue.toString(), key, operator, type);
					continue;
				}
				//处理like 关系符 end
				fieldValue = Tools.convert(fieldValue, type);
			}
			else
			{
				// 具有类型参数条件 Columns[=] or  Columns[no]
				Pattern patterOne = Pattern.compile("(.+?)\\[(.+)]");
				Matcher maecherOne = patterOne.matcher(fieldName);
				if(maecherOne.matches())
				{
					key = maecherOne.group(1);
					operator = maecherOne.group(2);
					//当才操作符为on表示不处理
					if("no".equals(operator))
					{
						continue;
					}
					//处理in , not in 关系符  begin
					if(StaticField.IN.equalsIgnoreCase(operator) || StaticField.NOTIN.equalsIgnoreCase(operator))
					{
						handleContain(fieldValue.toString(), key, operator, null);
						continue;
					}
					//处理in , not in 关系符  end
					//处理like 关系符 begin
					if(StaticField.LIKE.equalsIgnoreCase(operator))
					{
						handleVague(fieldValue.toString(), key, operator, "str");
						continue;
					}
				}
			}
			//组装查询条件
			queryCriteria.append(StaticField.SPACE).append(key).append(StaticField.SPACE).append(operator).append(StaticField.SPACE).append(StaticField.PLACEHOLDER);
			//添加对应的值
			params.add(fieldValue);
			if(index < size)
			{
				queryCriteria.append(" and ");
			}
		}
	}
	/**
	 * 处理包含关系 in , not in
	 * @param fieldValue 原数据
	 * @param key name值
	 * @param operator 关系符
	 * @param type 参数类型
	 */
	private void handleContain(String fieldValue,String key,String operator,String type)
	{
		queryCriteria.append(StaticField.SPACE).append(key).append(StaticField.SPACE).append(operator).append(StaticField.SPACE).append("(");
		String[] values = fieldValue.split(",");
		for (int i = 0,length = values.length; i < length; i++) {
			queryCriteria.append(StaticField.PLACEHOLDER);
			if(i < length-1)
			{
				queryCriteria.append(",");
			}
			//添加对应的值
			if(type == null)
			{
				params.add(values[i]);
			}
			else
			{
				params.add(Tools.convert(values[i], type));
			}
			
		}
		queryCriteria.append(")");
	}
	/**
	 * 处理模糊查询
	 * @param fieldValue 原数据
	 * @param key 值
	 * @param operator 关系符
	 * @param type 参数类型
	 */
	private void handleVague(String fieldValue,String key,String operator,String type)
	{
		if(!"str".equalsIgnoreCase(type) && !"string".equalsIgnoreCase(type))
		{
			throw new IllegalArgumentException("----------------模拟查询只试用与String类型的数据--------------- ");
		}
		
		queryCriteria.append(StaticField.SPACE).append(key).append(StaticField.SPACE).append(operator).append(StaticField.SPACE)
		.append(StaticField.PLACEHOLDER);
		params.add(fieldValue+"%");
	}
	/**
	 * 获取条件列
	 * @return
	 */
	public StringBuffer getFieldsColumns()
	{
		return queryCriteria;
	}
	/**
	 * 获取条件值
	 * @return
	 */
	public List<Object> getValueArray()
	{
		return params;
	}
	/**
	 * 获取页面添加集(修改、删除操作时可用)
	 * @return
	 */
	public Map<String, Object> getWhereMap() {
		return whereMap;
	}
	
	
}
