package cn.le.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.WebUtils;

import cn.le.page.DataRequest;
import cn.le.util.StaticField;


/**
 *<P>基础参数控制器,里面包含了各种需要用到的对象及基本参数<br/>
 *@author lelinked
 *@version  0.1
 *@date 2017-02-13
 */
@Controller("org_baseController")
@Scope("request")
public class BaseController {

	/**
	 * 导出行数
	 */
	private static Integer maxCount = null;
	/**
	 * HttpServletRequest
	 */
	protected HttpServletRequest request;
	/**
	 * HttpServletResponse
	 */
	protected HttpServletResponse response;
	/**
	 * 封装页面数据集
	  */
	protected Map<String,Object> paramMap;
	
	/**
	 * ServletResponseAware 接口的方法 初始化Action实例时容器时调用此方法
	 */
	@Autowired
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	/**
	 * ServletRequestAware 接口的方法 初始化Action实例时容器会时调用此方法
	 */
	@Autowired
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
		paramMap = WebUtils.getParametersStartingWith(request,StaticField.PARAMETERPREFIX);
	}
	
	@Resource(name = "maxExportedCount")
	public void setMaxExportedCount(Map<String, Integer> maxExportedCount) {
		maxCount = Integer.parseInt(maxExportedCount.get("count").toString());
	}
	/**
	 * 获取分页大小
	 * @return
	 */
	private String getPageSize()
	{
		String pageSize = request.getParameter(StaticField.PAGESIZE);
		if(isExported())
		{
			pageSize = maxCount.toString();
		}
		return pageSize;
	}
	/**
	 * 判断是否是导出
	 * @return
	 */
	protected boolean isExported()
	{
		String export = request.getParameter(StaticField.EXPORT);
		if(export != null && StaticField.NULLSTR.equals(export))
		{
			return true;
		}
		return false;
	}
	/**
	 * 处理Request中的参数,并进行处理
	 * 对应使用DateRequest.getFieldsColumns(String queryStatement)()方法
	 * @return DateRequest
	 * @throws Exception 
	 */
	public DataRequest  getPageRquest(String tempQuerySmt) 
	{        
		return getPageRquest(tempQuerySmt,null);
		
	}
	/**
	 * 处理Request中的分页参数,并进行处理
	 * 对应使用DateRequest.getFieldsColumns()方法
	 * @param tempQuerySmt 查询语句
	 * @return DateRequest
	 */
	public DataRequest  getPageRquest(String tempQuerySmt,Object[] params) 
	{        
		//设置分页数据和排序参数
		DataRequest pageRequest = new DataRequest(request.getParameter(StaticField.PAGENUMBER),
				getPageSize(),request.getParameter(StaticField.SORTCOLUMNS),paramMap,tempQuerySmt,params);
		
		return pageRequest;
	}
	/**
	 * 处理request页面中的参数
	 * @return DataRequest
	 */
	public DataRequest getDataRequest()
	{
		DataRequest dateRquest = new DataRequest(paramMap);
		paramMap = null;
		return dateRquest;
	}
	
}
