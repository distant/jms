package cn.le.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import cn.le.page.DataRequest;
import cn.le.page.Page;
import cn.le.service.BaseService;
import cn.le.util.StaticField;

/**
 *<P>基础方法控制器,里面包含了一些基础方法<br/>
 *@author lelinked
 *@date 2017-02-13
 * @version  0.1
 */
@Controller("org_methodController")
@Scope("request")
public class MethodController extends BaseController{
	
	@Autowired
	@Qualifier("org_baseService")
	private BaseService baseService;

	/**
	 * 非分页查询数据,页面获取数据名:info
	 * @param tempQuerySmt
	 * @param params
	 */
	public void listByResult(String tempQuerySmt)
	{
		ResponseRequest(baseService.findList(this.getPageRquest(tempQuerySmt, null)));
	}
	/**
	 * 非分页查询数据,页面获取数据名:info
	 * @param tempQuerySmt
	 * @param params
	 */
	public void listByResult(String tempQuerySmt,Object[] params)
	{
		ResponseRequest(baseService.findList(this.getPageRquest(tempQuerySmt, params)));
	}
	/**
	 * 非分页查询数据,页面获取数据名:info
	 * @param tempQuerySmt
	 * @param params
	 */
	public void listByResult(DataRequest dataRequest)
	{
		ResponseRequest(baseService.findList(dataRequest));
	}
	/**
	 * 封装分页查询数据，
	 * @param querySQL
	 * @throws Exception
	 */
	public void listByLimit(String tempQuerySmt)  
	{
		listByLimit(tempQuerySmt,null);
	}
	/**
	 * 封装分页数据查询
	 * @param tempQuerySmt 查询语句
	 * @param params 自定义参数
	 */
	public void listByLimit(String tempQuerySmt,Object[] params)  
	{
		listByLimit(this.getPageRquest(tempQuerySmt, params));
	}
	/**
	 * 封装分页数据查询
	 * @param DataRequest 分页数据集
	 */
	public void listByLimit(DataRequest pageRequest)  
	{
		ResponseRequest(baseService.getPage(pageRequest));
	}
	/**
	 * 合计分页查询
	 * @param tempQuerySmt 查询语句
	 * @param numColumn 求和列
	 */
	public void listByLimitTotal(String tempQuerySmt,String sumColumn) 
	{
		listByLimitTotal(tempQuerySmt, sumColumn,null);
	}
	/**
	 * 合计分页查询
	 * @param tempQuerySmt 查询语句
	 * @param numColumn  求和列
	 * @param params 自定义参数集
	 */
	public void listByLimitTotal(String tempQuerySmt,String sumColumn,Object[] params) 
	{
		DataRequest pageRquest = getPageRquest(tempQuerySmt,params);
		pageRquest.setSumColumn(sumColumn);
		
		listByLimitTotal(pageRquest);
	}
	/**
	 * 合计分页查询
	 * @param pageRquest 分页数据集
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void listByLimitTotal(DataRequest pageRquest) 
	{
		//结果集
		Map<String, Object> result = baseService.getPageTotal(pageRquest);
		Page page = (Page) result.get(StaticField.PAGE);
		ResponseRequest(page);
				
		//合计数据
		List<Map<String,Object>> total = (List) result.get(StaticField.TOTAL);
		if(total != null && total.isEmpty())
		{
			for (Map<String, Object> map : total) {
				for (Map.Entry<String, Object> param : map.entrySet()) {
					//为了与数据列区别开,求和列在列明后面追加SUM
					request.setAttribute(param.getKey()+StaticField.SUM, param.getValue());
				}
			}
		}
	}
	/**
	 * 返回数据集
	 * @param page 查询结果集
	 */
	public <T> void ResponseRequest(T t)
	{	
		if(t instanceof Page)
		{
			//返回查询结果集
			request.setAttribute(StaticField.PAGEINFO, t);
		}
		else
		{
			request.setAttribute(StaticField.INFO, t);
		}
		//回填查询条件
		request.setAttribute(StaticField.OLDPARAMETER, paramMap);
	}
	
	/**
	 * 获取BaseService对象
	 * @return
	 */
	public BaseService getBaseService() {
		return baseService;
	}
}
