package cn.le.dao;

import java.util.List;
import java.util.Map;

 /**
  *<P>基础操作接口<br/>
  *@author lelinked
  *@date 2017-02-13
  *@version 0.1
  */
public interface BaseDao {

	/**
	 * 结果集查询接口
	 * @param tempQuerySmt 查询语句
	 * @param params	查询条件
	 * @return
	 */
	public List<?> findList(String tempQuerySmt,Object[] params);
	/**
	 * 实体查询接口
	 * @param tempQuerySmt 查询语句
	 * @param params	查询条件
	 * @return
	 */
	public <T> T findObject(String querySentence,Object[] params,Class<?> clas);
	/**
	 * 利用sql查询单个数据集
	 * @param querySentence
	 * @param params
	 * @return
	 */
	public Map<?,?> findMap(String querySentence,Object[] params);
	/**
	 * 查询总记录数
	 * @param tempQuerySmt 查询语句
	 * @param params	查询条件
	 * @return
	 */
	public int findCount(String tempQuerySmt,Object[] params);
	/**
	 * 业务操作方法
	 * @param executeSmt 操作语句
	 * @param params 参数集
	 */
	public void executeBySql(String executeSmt,Object[] params);
}
