package cn.le.page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.le.conditions.QueryConditions;
import cn.le.util.StaticField;
import cn.le.util.Tools;

/**
 *<P>页面参数数据基类<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
public class DataRequest implements Serializable {

	private static final long serialVersionUID = -3399934325189446016L;

	/**
	 * 过滤参数
	 */
	private QueryConditions filters;

	/**
	 * 页号码
	 */
	private int pageNumber;

	/**
	 * request参数
	 */
	private Map<String,Object> requestParam;
	/**
	 * 自定义查询参数
	 */
	private Object[] params;
	/**
	 * 原始查询语句
	 */
	private String tempQuerySmt;
	/**
	 * 求和列
	 */
	private String sumColumn;
	/**
	 * 分页大小
	 */
	private int pageSize;

	/**
	 * 排序的多个列,如: username desc,age asc
	 */
	private String sortColumns;
	
	/**
	 * 分组的多个列 
	 */
	private String groupColumns;
	/**
	 * 求和标示列默认1
	 */
	private String paramCount;
	/**
	 * 条件是否被处理标示
	 */
	private int mark;
	/**
	 * 私有构成器,不允许构成无参数的DataRequest对象
	 */
	@SuppressWarnings("unused")
	private DataRequest() {
	
	}
	/**
	 * 构造DataRequest数据操作对象
	 * @param requestParam request参数集
	 */
	public DataRequest(Map<String,Object> requestParam) {
		this.requestParam = requestParam;
		this.filters = new QueryConditions();
	}
	
	public Map<String, Object> getRequestParam() {
		return requestParam;
	}
	/**
	 * 构造DataRequest分页数据操作对象
	 * @param pageNumber 当前页面
	 * @param pageSize 分页大小
	 * @param sortColumns 排序列
	 * @param requestParam request参数集
	 * @param tempQuerySmt 查询语句
	 */
	public DataRequest(String pageNumber,String pageSize,String sortColumns,Map<String,Object> requestParam,String tempQuerySmt) {
		
		initPage(pageNumber, pageSize, sortColumns, requestParam,tempQuerySmt);

	}
	/**
	 * 构造DataRequest分页数据操作对象
	 * @param pageNumber 当前页面
	 * @param pageSize 分页大小
	 * @param sortColumns 排序列
	 * @param requestParam request参数集
	 * @param tempQuerySmt 查询语句
	 * @param params 自定义参数集
	 */
	public  DataRequest(String pageNumber,String pageSize,String sortColumns,Map<String,Object> requestParam,
			String tempQuerySmt,Object[] params) {
		
		initPage(pageNumber, pageSize, sortColumns, requestParam,tempQuerySmt);

		this.params = params;
	}
	/**
	 * 输出化分页数据
	 */
	private void initPage(String pageNumber,String pageSize,String sortColumns,Map<String,Object> requestParam,String tempQuerySmt)
	{
		this.pageNumber = Tools.strToInt(pageNumber, StaticField.DEFAULT_PAGE_NUMBER);
		this.pageSize = Tools.strToInt(pageSize, StaticField.DEFAULT_PAGE_SIZE);
		this.sortColumns = StaticField.ORDERBY + sortColumns;
		if(StringUtils.isEmpty(sortColumns))
		{
			this.sortColumns = "";
		}
		
		this.requestParam  = requestParam;
		this.tempQuerySmt = tempQuerySmt;
		this.filters = new QueryConditions();
	}
	/**
	 * 设置拍序列
	 * @param sortColumns
	 */
	public void setSortColumns(String sortColumns) {
		this.sortColumns =  StaticField.ORDERBY + sortColumns;
	}
	/**
	 * 设置拍序列
	 * @param sortColumns
	 */
	public String getSortColumns() {
		return sortColumns;
	}
	/**
	 * 设置分组列
	 * @param groupColumns
	 */
	public void setGroupColumns(String groupColumns) {
		this.groupColumns = StaticField.GROUPBY + groupColumns;
	}
	/**
	 * 获取分组列
	 * @param groupColumns
	 */
	public String getGroupColumns() {
		return groupColumns == null ? "":groupColumns;
	}
	/**
	 * 添加参数
	 */
	public void addColumn(String field, Object value)
	{
		requestParam.put(field, value);
		mark = 0;
	}
	/**
	 * 获取参数
	 */
	public Object getColumn(String field)
	{
		return requestParam.get(field);
	}
	/**
	 * 根据name,移除一个参数
	 * @param field[?]...
	 */
	public void removeColumn(String field)
	{
		requestParam.remove(field);
		mark = 0;
	}
	/**
	 * 获取查询语句
	 * @param queryStatement 基础查询语句
	 * @return
	 */
	public String getFieldsColumns(String queryStatement)
	{
		
		if(mark == 0)
		{
			filters.requestColumns(requestParam);
			mark = 1;
		}
		StringBuffer str = filters.getFieldsColumns();
		String smt = tempQuerySmt;
		if(StringUtils.isNotEmpty(queryStatement))
		{
			smt = queryStatement;
		}
		if( StringUtils.isNotEmpty(new String(str)))
		{
			smt = smt + Tools.isAppendAnd(tempQuerySmt)+str.toString();
		}
		return smt;
	}
	/**
	 * 获取查询语句
	 * @param queryStatement 基础查询语句
	 * @return
	 */
	public String getFieldsColumns()
	{
			return getFieldsColumns(null);
	}
	/**
	 * 获取条件值
	 * @return
	 */
	public Object[] getValueArray()
	{
		List<Object> reqParams = filters.getValueArray();
		if(reqParams == null)
		{
			throw new IllegalArgumentException("----------请先执行getFieldsColumns方法,进行处理条件列--------- ");
		}
		if(params != null)
		{
			//处理自定义的参数,自定义的参数应放在自动组装惨的数的前面
			List<Object>defParams = Arrays.asList(params);
			defParams = new ArrayList<Object>(defParams);
			 defParams.addAll(reqParams);
			 reqParams = defParams;
		}
	
		return reqParams.toArray();
	}
	
	/**
	 * 获取非分页页面参数
	 * @param requestParam
	 * @return
	 */
	public Map<String,Object> getColumnParameter()
	{
		return filters.getColumnParameter(requestParam);
	}
	/**
	 * 获取页面添加集(修改操作时可用),需要先执行getColumnParameter()获取非分页页面参数
	 * @return
	 */
	public Map<String, Object> getWhereMap() {
		return filters.getWhereMap();
	}
	/**
	 * 获取求和列
	 * @return
	 */
	public String getSumColumn() {
		return sumColumn;
	}
	/**
	 * 添加求和列
	 * @param numColumn
	 */
	public void setSumColumn(String sumColumn) {
		this.sumColumn = sumColumn;
	}
	/**
	 * 添加子定义参数
	 * @param params
	 */
	public void setValueArray(Object[] params)
	{
		this.params = params;
	}
	/**
	 * 获取分页大小
	 * @return
	 */
	public int getPageNumber() {
		return pageNumber;
	}
	/**
	 * 获取当前页码
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}
	/**
	 * 获取合计标示列
	 * @return
	 */
	public String getParamCount() {
		return paramCount == null ? "1" : paramCount;
	}
	/**
	 * 定义合计标示列
	 * @param paramCount
	 */
	public void setParamCount(String paramCount) {
		this.paramCount = paramCount;
	}
}




