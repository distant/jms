package cn.le.page;

import java.util.List;


/**
 *<P>分页数据结果集<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
public class Page  {

	/**
	 * 查询结果记录集
	 */
	protected List<?> result;

	/**
	 * 每一页显示的条目数
	 */
	protected int pageSize;

	/**
	 * 当前页码
	 */
	protected int pageNumber;

	/**
	 * 总的数据条目数量
	 */
	protected int totalCount = 0;

	/**
	 * 分页信息
	 * 
	 * @param pageNumber 当前页码
	 * @param pageSize 每一页显示的条目数
	 * @param totalCount 总的数据条目数量
	 * @param result 查询结果记录集
	 */
	public Page(int pageNumber, int pageSize, int totalCount, List<?> result) {
		if (pageSize <= 0)
		{
			throw new IllegalArgumentException("--------Page--每一页显示的条目数不能小于0");
		}
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		this.totalCount = totalCount;
		setResult(result);
	}

	/**
	 * 设置查询结果记录集
	 * @param 
	 */
	public void setResult(List<?> elements) {
		this.result = elements;
	}

	/**
	 * 返回查询结果记录集
	 * @return
	 */
	public List<?> getResult() {
		return result;
	}

	/**
	 * 总的数据条目数
	 * @return 
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * 设置总的数据条目数
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * 获取每一页显示的条目数
	 * @return 
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置每一页显示的条目数
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 获取当前页的页码
	 * @return 
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * 设置当前页的页码
	 * @param pageNumber
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * 获取总页数
	 * @return 
	 */
	public int getTotalPage() {
		int totalPage = 1;
		if (totalCount % pageSize == 0) {
			totalPage = totalCount / pageSize;
		} else {
			totalPage = totalCount / pageSize + 1;
		}
		return totalPage;
	}
}
