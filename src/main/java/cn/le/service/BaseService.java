package cn.le.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import cn.le.conditions.Handle;
import cn.le.dao.BaseDao;
import cn.le.page.DataRequest;
import cn.le.page.Page;
import cn.le.util.StaticField;

/**
 *<P>业务操作基础服务<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
@Service("org_baseService")
@Scope("request")
public class BaseService {

	@Autowired
	private Handle handle;
	@Autowired
	@Qualifier("org_baseDao")
	private BaseDao baseDao;
	
	/**
	 * 分页查询
	 * @param pageRequest 分页数据集
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Page getPage(DataRequest pageRequest)
	{
		//处理数据集
		handle.setPageRequest(pageRequest);
		
		//分页查询
		List resultList = baseDao.findList(handle.getPagingSentence(), pageRequest.getValueArray());
		//总数查询
		int count = baseDao.findCount(handle.getCountSentence(), pageRequest.getValueArray());
		//返回结果集
		return new Page(pageRequest.getPageNumber(),pageRequest.getPageSize(),count,resultList);
		
	}
	/**
	 * 合计分页查询
	 * @param pageRequest 分页数据集
	 * @return map.get("page"):分页数据 map.get("total"):合计数据集
	 */
	public Map<String,Object> getPageTotal(DataRequest pageRequest)
	{
		//查询结果集
		Page page = this.getPage(pageRequest);
		//查询合计
		List<?> total = baseDao.findList(handle.getSumSentence(), pageRequest.getValueArray());
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(StaticField.PAGE, page);
		result.put(StaticField.TOTAL, total);
		return result;
	}
	/**
	 * 利用DataRequest封装查询数据进行非分页查询 
	 * @param dataRequest 查询数据对象
	 * @return
	 */
	public List<?> findList(DataRequest dataRequest)
	{
		return baseDao.findList(dataRequest.getFieldsColumns()+dataRequest.getSortColumns(), dataRequest.getValueArray());
	}
	/**
	 * 利用DataRequest封装查询单个数据集查询
	 * @param dataRequest 查询数据对象
	 * @return
	 */
	public Map<?,?> findMap(DataRequest dataRequest)
	{
		return this.findMap(dataRequest.getFieldsColumns(), dataRequest.getValueArray());
	}
	/**
	 * 利用DataRequest封装查询是对数据对象
	 * @param dataRequest 查询数据对象
	 * @param Class 实体class对象
	 * @return
	 */
	public <T> T findObject(DataRequest dataRequest,Class<?> cla)
	{
		return this.findObject(dataRequest.getFieldsColumns(), dataRequest.getValueArray(),cla);
	}
	/**
	 * 利用sql查询数据结果集
	 * @param querySentence 查询语句
	 * @param params	条件参数
	 * @return
	 */
	public List<?> findList(String querySentence,Object[] params)
	{
		return  baseDao.findList(querySentence,params);
		
	}
	/**
	 * 利用sql查询数据结果集
	 * @param querySentence 查询语句
	 * @param params	条件参数
	 * @return
	 */
	public List<?> findList(String querySentence)
	{
		return  this.findList(querySentence,null);
	}
	/**
	 * 利用sql查询实体对象
	 * @param querySentence 查询语句
	 * @param params	条件参数
	 * @param Class 实体的class对象
	 * @return
	 */
	public <T> T findObject(String querySentence,Object[] params,Class<?> clas)
	{
		return baseDao.findObject(querySentence, params,clas);
	}
	/**
	 * 利用sql查询实体对象
	 * @param querySentence 查询语句
	 * @param Class 实体的class对象
	 * @return
	 */
	public <T> T findObject(String querySentence,Class<?> clas)
	{
		return findObject(querySentence,null,clas);
	}
	/**
	 * 利用sql查询单个数据集
	 * @param querySentence
	 * @param params
	 * @return
	 */
	public Map<?,?> findMap(String querySentence,Object[] params)
	{
		return baseDao.findMap(querySentence,params);
	}
	/**
	 * 利用sql查询单个数据集
	 * @param querySentence
	 * @param params
	 * @return
	 */
	public Map<?,?> findMap(String querySentence)
	{
		return this.findMap(querySentence,null);
	}
	/**
	 * 新增操作
	 * @param dataRequest 数据集
	 * @param tableName 新增表名
	 */
	public void executeByInsert(DataRequest dataRequest,String tableName)
	{
		handle.insertHand(dataRequest.getColumnParameter(), tableName);
		executeBySql(handle.getExecuteSentence(),handle.getParamsArray());
	}
	/**
	 * 修改操作
	 * @param dataRequest 数据集
	 * @param tableName 新增表名
	 */
	public void executeByUpdate(DataRequest dataRequest,String tableName)
	{
		handle.updateHand(dataRequest.getColumnParameter(), tableName,dataRequest.getWhereMap());
		executeBySql(handle.getExecuteSentence(),handle.getParamsArray());
	}
	/**
	 *  
	* 删除操作
	* @param  dataRequest 数据集
	* @param  tableName    删除表名
	* @return void    
	 */
	public void executeByDelete(DataRequest dataRequest,String tableName)
	{
		dataRequest.getColumnParameter();
		handle.deleteHand(tableName, dataRequest.getWhereMap());
		executeBySql(handle.getExecuteSentence(),handle.getParamsArray());
	}
	/**
	 * 业务执行操作
	 */
	public void executeBySql(String executeSentence,Object[] params){
		baseDao.executeBySql(executeSentence,params);
	}
}
