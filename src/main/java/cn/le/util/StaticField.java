package cn.le.util;

/**
 *<P>静态字符<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
public class StaticField {

	/**
	 * like
	 */
	public static String LIKE = "like";
	/**
	 *  not in
	 */
	public static String NOTIN = "not in";
	/**
	 *  in 
	 */
	public static String IN = "in";
	/**
	 * total
	 */
	public static String TOTAL = "total";
	/**
	 * page
	 */
	public static String PAGE = "page";
	/**
	 * SUM
	 */
	public static String SUM = "SUM";
	/**
	 * export
	 */
	public static String EXPORT  = "export";
	/**
	 * order by 
	 */
	public static String ORDERBY = " order by ";
	/**
	 * group by 
	 */
	public static String GROUPBY  = " group by  ";
	/**
	 * 分页大小的变量名
	 */
	public static final String PAGESIZE = "pageSize";

	/**
	 * 当前页码的变量名
	 */
	public static final String PAGENUMBER = "pageNo";

	/**
	 * 排序列的变量名
	 */
	public static final String SORTCOLUMNS = "sortColumns";
	/**
	 * 默认每页15条
	 */
	public static final int DEFAULT_PAGE_SIZE = 100;

	/**
	 * 默认第一页
	 */
	public static final int DEFAULT_PAGE_NUMBER = 1;
	/**
	 * 变量开始前缀的变量名
	 */
	public static final String PARAMETERPREFIX = "org_";
	/**
	 * 分页页面信息变量名
	 */
	public static final String PAGEINFO = "pageInfo";
	/**
	 * 非分页查询返回页面信息
	 */
	public static final String INFO = "info";
	/**
	 * 页面信息变量名
	 */
	public static final String OLDPARAMETER = "oldParams";
	/**
	 * 空字符
	 */
	public static String NULLSTR = "";
	/**
	 * 空格
	 */
	public static String  SPACE = " ";
	/**
	 * 占位符
	 */
	public static String PLACEHOLDER = " ? ";
	
}
