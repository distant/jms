package cn.le.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *<P>数据类型处理工具类<br/>
 *@author lelinked
 *@date 2017-02-13
 *@version 0.1
 */
public class Tools {

	/**
	 * 数据类型转换
	 * @param value 值
	 * @param type  需要转换的类型
	 */
	public static Object convert(Object value,String type)
	{
		String valuesStr =  value.toString();
		
		if(value instanceof Date)
		{
			return  value;
		}
		
		if("date".equalsIgnoreCase(type))
		{
			SimpleDateFormat format =   new SimpleDateFormat("yyyy-MM-dd");
			try {
				return  format.parse(valuesStr);
			} catch (ParseException e) {
				e.printStackTrace();
				
			}
		}
		if("datetime".equalsIgnoreCase(type))
		{
			 SimpleDateFormat format =   new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				return  format.parse(valuesStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if("int".equalsIgnoreCase(type))
		{
			return Integer.parseInt(valuesStr);
		}
		if("double".equalsIgnoreCase(type))
		{
			return Double.parseDouble(valuesStr);
		}
		if("long".equalsIgnoreCase(type))
		{
			return Long.parseLong(valuesStr);
		}
		if("string".equalsIgnoreCase(type) || "str".equalsIgnoreCase(type))
		{
			return valuesStr;
		}
		 throw new IllegalArgumentException("----------------参数类型不匹配--------------- "+type);
	}
	
	/**
	 * 将字符串转换成int
	 * 
	 * @param str
	 * @param defaultV
	 *            出现异常后 默认值
	 * @return
	 */
	public static int strToInt(String str, int defaultV) {
		int result = defaultV;
		try {
			if (str != null && str.length() != 0)
				result = Integer.parseInt(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * 验证sql是否可以在后面拼接and
	 * @param sql 
	 * @return sql
	 */
	public static String isAppendAnd(String sql)
	{
		if(sql.contains("(") && sql.contains(")"))
		{
			char a = '(';
			char b = ')';
			while (true) {
				List<Integer> lsit1 =  new ArrayList<Integer>();
				List<Integer> lsit2 =  new ArrayList<Integer>();
				for (int i = 0; i < sql.length(); i++) {
					char k = sql.charAt(i);
					if(k == a)
					{
						lsit1.add(i);
					}else if(k == b)
					{
						lsit2.add(i);
					}
					//验证保存( 和 )下标的list不为null
					if(!lsit1.isEmpty() && !lsit2.isEmpty())
					{
						//如果相等就返回
						if(lsit1.size() == lsit2.size())
						{
							break;
						}
					}
				}
				sql = sql.substring(0,lsit1.get(0)) + sql.substring(lsit2.get(lsit2.size()-1)+1,sql.length());
				if(!sql.contains("(") && !sql.contains(")"))
				{
					break;
				}
			}
		}
		if(sql.contains("where"))
		{
			return   " and ";
		}
		return  " where ";
	}
}
